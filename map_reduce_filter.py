#
# Map, Reduce, Filter
#

x = lambda n1: n1**2
y = lambda n1, n2: n1+n2
z = lambda n1: n1>30
lista=[2,3,4,5,6,24,52,66,161,70,11]

# MAP
print(list(map(x,lista)))

# REDUCE
from functools import reduce
print(reduce(y,lista))

# FILTER
print(list(filter(z, lista)))


