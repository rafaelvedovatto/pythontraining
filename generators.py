#
# Generators - usado para gerar conteúdo
#

def sequencia(numero):
    lista=[]
    for item in range(numero):
        lista.append(item)
    return lista

#print(sequencia(86754068934506)) #da ruim!
print(sequencia(20))


def gen_sequencia(numero):
    for item in range(numero):
        yield item

#print(gen_sequencia(20))
numero_generator = gen_sequencia(10)
print(numero_generator)
print(next(numero_generator))
print(next(numero_generator))
for x in numero_generator:
    if x % 2 == 0:
        print(x)


