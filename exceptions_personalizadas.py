#
# Exceptions personalizadas
#

class NaoEstaNaLista(Exception):

    def __init__(self,nome):
        self.message = nome


    def __str__(self) -> str:
        print(f"Não esta na lista: nome recebido {self.message}")


lista = ["A","B","C"]

pessoa = input("Qual o seu nome? ")

if pessoa not in lista:
    raise NaoEstaNaLista(pessoa)


