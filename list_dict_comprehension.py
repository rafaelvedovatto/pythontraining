#
# List comprehension
#
lista = []
for numero in range(5,10):
    lista.append(numero * numero)
print(lista)

lista = [numero * numero for numero in range(5,10)]
print(lista)


#
# Dict comprehension
#
dicionario={}
for numero in range(4,8):
    dicionario[numero] = numero * numero
print(dicionario)

dicionario = {chave:valor*valor for chave,valor in enumerate(range(4,8))}
print(dicionario)

frutas = ["maça","banana","abacate"]
for item in enumerate(frutas):
    print(item)
for chave, item in enumerate(frutas):
    print(chave,item)