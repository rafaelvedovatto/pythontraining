# Class / Classes

class Pilha:
    # def __init__(self) -> None:
    def __init__(self):
        self.__pilha=[] #se __ preceder uma variável então o escopo dela é local (encapsulado)
        self.__topo=0

    def empilhar(self, item):
        self.__pilha.append(item)
        self.__topo += 1

    def desempilhar(self):
        if self.__topo > 0:
            ultimo_item = self.pilha[-1] #quando passando um valor negativo a lista é lida de tras pra frente
            self.__pilha.remove(ultimo_item)
            self.__topo -= 1
            return "Item removido."
        else:
            return "Nenhum item empilhado."

    def checar(self):
        return self.__pilha

balcao = Pilha()

balcao.empilhar("Pato de vidro")
balcao.empilhar("Pato de metal")
balcao.empilhar("Pato de alumínio")

print(balcao.pilha)

balcao.desempilhar()
print(balcao.pilha)

# herança

class Funcionario:
    def __init__(self) -> None:
        self.__nome = ""
        self.__idade = 0
        self.__salario = 0


class Gerente(Funcionario):
    def __init__(self) -> None:
        super().__init__() #gerente vai herdar os atributos de funcionario (nome, idade, salario)
        self.__bonus = "25%"


# Polimorfismo
class Cliente:
    def __init__(self) -> None:
        self.carrinho=[]
        self.cpf=''
        self.total=0

    def adicionar_item(self, item):
        self.carrinho.append(item)

    def caixa(self):
        for item in self.carrinho:
            self.total += item.preco
        return self.total


class ClienteVIP(Cliente):
    def __init__(self) -> None:
        super().__init__()
        self.desconto = 0.95

    def caixa(self):
        for item in self.carrinho:
            self.total += item.preco
        return self.total*self.desconto
