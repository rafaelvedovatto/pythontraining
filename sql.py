# Bancos de Dados - Databases - DBs

import sqlite3 # Modulo utilizado pra criação e gerencia de bancos SQL no python.

conexao = sqlite3.connect("estoque.db")  # Estabelece uma conexão ou cria um novo arquivo se ele não existir.
cursor = conexao.cursor() # Cria uma ferramenta pra editarmos o banco de dados.

# SQL armazena suas informações em tabelas.

# Comando pra criar uma tabela chamada "nomes":
comando_cria_tabela = """
CREATE TABLE nomes (
id integer primary key autoincrement,
nome text,
idade integer,
sexo)
"""

try:
    cursor.execute(comando_cria_tabela) #Executa o comando no banco de dados pra criar a tabela.
except sqlite3.OperationalError:
    pass

letra = "M"

#Como adicionar itens em uma tabela em um banco de dados sql:
comando_adiciona_item = f"""
INSERT INTO nomes (nome, idade, sexo)
values ("Tiago", 27, "{letra}")
"""

cursor.execute(comando_adiciona_item)
conexao.commit()

#Como remover um item de uma tabela em um banco de dados sql:
comando_remove_item = """
DELETE FROM nomes WHERE id = 2
"""

cursor.execute(comando_remove_item)
conexao.commit()

#Como atualizar um campo de um item dentro de uma tabela em um banco sql:
comando_atualiza_valor = """
UPDATE nomes
SET nome = "Caio"
WHERE id = 3
"""

cursor.execute(comando_atualiza_valor)
conexao.commit()

#Como checar o conteudo de dentro de uma tabela em um banco de dados sql:
comando_checa_tabela = """
SELECT * FROM nomes
"""

cursor.execute(comando_checa_tabela)
conteudo = cursor.fetchall()
print(conteudo)
