# Manipulação de arquivos

# r -> READ
# w -> WRITE
# a -> APPEND

#arquivo = open("texto.txt","w")
#conteudo = "Primeira linha"
# arquivo = open("texto.txt","a")
#conteudo = "\nSegunda linha"
#conteudo = "\nTerceira linha\n"
# conteudo = "Quarta linha\n"
# arquivo.write(conteudo)
# arquivo.close()

arquivo = open("texto.txt","r")
linhas = arquivo.readlines()
print(linhas) #imprime as linhas e da pra ver que elas estão em um formato de lista
for linha in linhas:
    print(linha.strip())
    #outra forma
    print(linha, end="")


import csv

with open("registro.csv","r") as arquivo:
    conteudo = csv.reader(arquivo, delimiter=";")
    cabecalho=next(conteudo)
    primeira_linha = next(conteudo)
    segunda_linha = next(conteudo)
    terceira_linha = next(conteudo)
    # quarta_linha = next(conteudo) # dá erro pq EOF
    # print(cabecalho,primeira_linha,segunda_linha,terceira_linha,quarta_linha)
    print(cabecalho,primeira_linha,segunda_linha,terceira_linha)
    for linha in conteudo:
        print(linha)
        #print(next(conteudo))
arquivo.close()

with open("registro.csv","r") as arquivo:
    conteudo = csv.reader(arquivo, delimiter=";")

    print(conteudo)

    lista_conteudo=[]
    for linha in conteudo:
        lista_conteudo.append(linha)
    
    print(lista_conteudo)
arquivo.close()

# usar o newline para adicionar as entrada em uma nova linha e deixar tb uma nova linha no arquivo
with open("registro.csv","a",newline="") as arquivo:

    escrita = csv.writer(arquivo, delimiter=";")

    escrita.writerow(["12323434545","Rafael","35","M","Brasileiro"])
    
arquivo.close()

with open("registro.csv","r") as arquivo:
    conteudo = csv.reader(arquivo, delimiter=";")

    for linha in conteudo:
        print(linha)

# usar o newline para adicionar as entrada em uma nova linha e deixar tb uma nova linha no arquivo
with open("registro.csv","a+",newline="") as arquivo:

    escrita = csv.writer(arquivo, delimiter=";")

    escrita.writerow(["12323434545","Rafael","35","M","Brasileiro"])

    conteudo = csv.reader(arquivo, delimiter=";")

    for linha in conteudo:
        print(linha)
    
arquivo.close()

