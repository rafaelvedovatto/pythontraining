# ==========================================================================
# 2) Implemente um programa que represente uma fila. O contexto do programa é uma
# agência de banco. Cada cliente ao chegar deverá respeitar a seguinte regra: o primeiro
# a chegar deverá ser o primeiro a sair. Você poderá representar pessoas na fila a par-
# tir de números os quais representam a idade. A sua fila deverá conter os seguintes
# comportamentos:

# • Adicionar pessoa na fila: adicionar uma pessoa na fila.
# • Atender Fila: atender a pessoa respeitando a ordem de chegada
# • Dar prioridade: colocar uma pessoa maior de 65 anos como o primeiro da fila

class FilaDeBanco:
    
    def __init__(self) -> None:
        self.__fila=[]
    
    def adicionar_a_fila(self, idade):
        i=0
        indexidosos=0
        if idade < 65:
            self.__fila.append(idade)
        else:
            if len(self.__fila) == 0:
                self.__fila.append(idade)
            else:
                for idosos in self.__fila:
                    if idosos >= 65:
                        indexidosos += 1
                if indexidosos > 0:
                    self.__fila.insert(indexidosos,idade)
                else: 
                    self.__fila.insert(0,idade)
        print(self.__fila)

    def atender(self):
        if len(self.__fila) > 0:
            quemsaiu = self.__fila.pop(0)
            print(f"Pessoa {quemsaiu} foi atendida")
        else:
            print("A fila está vazia!")
        #print(self.__fila)
        
banco = FilaDeBanco()
banco.adicionar_a_fila(31)
banco.adicionar_a_fila(65)
banco.adicionar_a_fila(64)
banco.adicionar_a_fila(68)
banco.atender()
banco.atender()
banco.atender()
banco.atender()
banco.atender()