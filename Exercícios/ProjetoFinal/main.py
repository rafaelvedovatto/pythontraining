# Utilizando todo o conteúdo que aprendeu no treinamento, crie uma aplicação capaz de simular o 
# funcionamento de um caixa eletrônico. O caixa deverá possuir as seguintes funções:
#
# - Registrar novos clientes
# - Fazer login dos clientes
# - Checar o saldo dos clientes
# - Realizar depósitos
# - Realizar saques

# Faça o uso de classes, funções e tratamentos de erro para otimizar seu código.

# - Clientes podem ser criados a partir de uma classe
# - informações dos clientes como nome, senha e saldo podem ser armazenadas em um banco de dados.
# - Podemos criar funções para tomar as acções do caixa eletrônico
# - Podemos fazer uso de diversos arquivos para segmentar o código

# 1- Criar um arquivo para lidar com banco de dados
# 2- Criar as tabelas do DB.
#   - Tabela clientes (nome, idade, saldo)
#   - Tabela usuário/senha
# 3- Criar um arquivo que conterá as funções executadas pelo programa (log)
# 4- No main.py criar um menu para mostrar ao usuário


from lib import funcoes,classes


def main():
    while True:
        opc = ""
        cerquinhas="#######################################"
        print(cerquinhas)
        print("###            CAIXA 24H            ###")
        print(cerquinhas)
        print("### 1 - Registrar novos clientes    ###")
        print("### 2 - Fazer login dos clientes    ###")
        print("### 3 - Checar o saldo dos clientes ###")
        print("### 4 - Realizar depósitos          ###")
        print("### 5 - Realizar saques             ###")
        print("### 6 - Sair                        ###")
        print(cerquinhas)
        try:
            opc = input("Selecione uma das opções acima: ")
            if opc.isnumeric == True and len(opc) == 1:
                match opc:
                    case "1":
                        funcoes.cadastro()
                    case "2":
                        pass
                    case "3":
                        pass
                    case "4":
                        pass
                    case "5":
                        pass
                    case "6":
                        break
                    case _:
                        print("Ixi!")
            else:
                funcoes.limpatela()
                print("Opção informada é inválida.")    
        except ValueError:
            funcoes.limpatela()
            print("Opção informada é inválida.")



if __name__ == "__main__":
    main()
