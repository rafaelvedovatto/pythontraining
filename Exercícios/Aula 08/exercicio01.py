# Reescreva o exercicio 4 da aula 05 adicionando tratamentos de erro, 
# com a finalidade de que o programa nunca dê uma exceção e também que ele
# não aceite dados incorretos em nenhum momento.

import csv
import os

def Cadastro():
    cpf=""
    nome=""
    idade=""
    sexo=""
    endereco=""
    dados=[]
    cpf=input("Informe o CPF: ")
    validadados(cpf,tipodado="cpf")
    nome=input("Informe o nome: ")
    validadados(nome,tipodado="nome")
    idade=input("Informe a idade: ")
    validadados(idade,tipodado="idade")
    sexo=input("Informe o sexo: (m/f) ")
    validadados(sexo,tipodado="sexo")
    endereco=input("Informe o endereço: ")
    validadados(endereco,tipodado="end")
    jahcadastrado = consultaUsuario(cpf)
    if jahcadastrado == False:
        dados.append(cpf)
        dados.append(nome)
        dados.append(idade)
        dados.append(sexo)
        dados.append(endereco)
        gravaDados(dados)


def validadados(valor,tipodado=""):
    while True:
        if tipodado=="cpf":
            if len(valor) == 11 and valor.isnumeric():
                break
            else:
                print("CPF informado é inválido.")
                valor=input("Informe o CPF: ")
        if tipodado=="nome":
            if len(valor) > 0 and valor.isalpha():
                break
            else:
                print("Nome informado é inválido.")
                valor=input("Informe o nome: ")
        if tipodado=="idade":
            if int(valor) > 0 and valor.isnumeric():
                break
            else:
                print("Idade informada é inválido.")
                valor=input("Informe a idade: ")
        if tipodado=="sexo":
            if len(valor) == 1 and valor.isalpha() and (valor.lower()=="m" or valor.lower()=="f"):
                break
            else:
                print("Sexo informado é inválido.")
                valor=input("Informe o sexo: (m/f) ")
        if tipodado=="end":
            if len(valor) > 0 and valor.isalnum() == True:
                break
            else:
                print("Endereço informado é inválido.")
                valor=input("Informe o endereço: ")


def gravaDados(dados):
    with open("registro2.csv","a",newline="") as arquivo:
        escrita = csv.writer(arquivo, delimiter=";")
        escrita.writerow(dados)
        os.system('cls')
        print(f"Cadastro realizado com sucesso!")
    arquivo.close()


def consultaUsuario(cpf):
    with open("registro2.csv","r") as arquivo:
        registros = csv.reader(arquivo, delimiter=";")
        for linha in registros:
            #print(linha)
            if linha[0] == cpf:
                print(f"{cpf} já cadastrado.")
                #print(f"Dados: {linha}")
                print(f" - CPF: {linha[0]}")
                print(f" - Nome: {linha[1]}")
                print(f" - Idade: {linha[2]}")
                print(f" - Sexo: {linha[3]}")
                print(f" - Endereço: {linha[4]}")
                achou=True
                return achou
            else:
                achou = False
    return achou

def exclusao(cpf):
    with open("registro2.csv","r") as arquivo_inicial, open("temp_file.csv","a") as  arquivo_final:
        saida = csv.writer(arquivo_final,delimiter=";")
        for linha in csv.reader(arquivo_inicial,delimiter=";"):
            print(f"for {linha}")
            if linha[0] != cpf:
                print(f"if {linha}")
                saida.writerow(linha)
    os.remove("registro2.csv")
    os.rename("temp_file.csv","registro2.csv")
    arquivo_final.close()
    arquivo_inicial.close()

def main():
    # cria o arquivo e imprime o header
    # with open("registro2.csv","w",newline="") as arquivo:
    #     escrita = csv.writer(arquivo, delimiter=";")
    #     escrita.writerow(["cpf,nome,idade,sexo,nacionalidade"])
    # arquivo.close()
    
    #os.remove("temp_file.csv")

    while True:
        #os.system('cls')
        linha="###########################################"
        print(linha)
        print(f"###    Painel de cadastro de usuário    ###")
        print(f"### Selecione uma das opções abaixo:    ###")
        print(f"### 1- Cadastrar novo usuário           ###")
        print(f"### 2- Consultar cadastro               ###")
        print(f"### 3- Excluir cadastro                 ###")
        print(f"### 4- Sair                             ###")
        menu=input("")
        if menu=="1":
            Cadastro()
        elif menu=="2":
            print(linha)
            print(f"###    Painel de consulta de usuário    ###")
            print(f"### Informe os dados abaixo:            ###")
            print(f"###                                     ###")
            cpf=input("### CPF: ")
            achou = consultaUsuario(cpf)
            if achou != True:
                print(f"CPF {cpf} não cadastrado.")
        elif menu=="3":
            print(f"###    Painel de exclusão de usuário    ###")
            print(f"### Informe os dados abaixo:            ###")
            print(f"###                                     ###")
            cpf=input("### CPF: ")
            exclusao(cpf)
        else:
            break
     

if __name__ == '__main__':
    main()


