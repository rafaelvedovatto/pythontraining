#====================================================================================
# 2) Escreva um programa em python que realize um cadastro. Deverão ser coletadas as
# seguintes informações:

# CPF
# Nome
# Idade
# Sexo
# Endereço

# Os registros deverão ser armazenados em um arquivo CSV. Caso desejar manter o padrão
# brasileiro, o CSV será separado pelo caractere ;

import csv

def Cadastro(dados):
    with open("registro2.csv","a",newline="") as arquivo:

        escrita = csv.writer(arquivo, delimiter=";")

        escrita.writerow(dados)

        print(f"Cadastro realizado com sucesso!")
    
    arquivo.close()

def validaDados(valor,tipodado=""):
    if tipodado=="cpf":
        if len(valor) == 11 and valor.isnumeric():
            return True
        else:
            print("CPF informado é inválido.")
            return False
    if tipodado=="nome":
        if len(valor) > 0 and valor.isalpha():
            return True
        else:
            print("Nome informado é inválido.")
            return False
    if tipodado=="idade":
        if int(valor) > 0 and valor.isnumeric():
            return True
        else:
            print("Idade informada é inválido.")
            return False
    if tipodado=="sexo":
        if len(valor) == 1 and valor.isalpha() and (valor.lower()=="m" or valor.lower()=="f"):
            return True
        else:
            print("Sexo informado é inválido.")
            return False
    if tipodado=="nacionalidade":
        if len(valor) > 0 and valor.isalpha():
            return True
        else:
            print("Nome informado é inválido.")
            return False

def main():
    
    cpf_valido=False
    nome_valido=False
    idade_valida=False
    sexo_valido=False
    nacionalidade_valida=False
    cpf=""
    nome=""
    idade=""
    sexo=""
    nacionalidade=""
    dados=[]

    # cria o arquivo e imprime o header
    # with open("registro2.csv","w",newline="") as arquivo:
    #     escrita = csv.writer(arquivo, delimiter=";")
    #     escrita.writerow(["cpf,nome,idade,sexo,nacionalidade"])
    # arquivo.close()
    while True:
        print("############################")
        print("### Cadastro de usuário: ###")
        if cpf_valido == False:
            cpf=input("Informe o CPF: ")
            print(cpf,len(cpf),cpf.isnumeric())
        if nome_valido == False:
            nome=input("Informe o nome: ")
        if idade_valida == False:
            idade=input("Informe a idade: ")
        if sexo_valido == False:
            sexo=input("Informe o sexo: (m/f) ")
        if nacionalidade_valida == False:
            nacionalidade=input("Informe a nacionalidade: ")
        cpf_valido = validaDados(cpf,tipodado="cpf")
        nome_valido = validaDados(nome,tipodado="nome")
        idade_valida = validaDados(idade,tipodado="idade")
        sexo_valido = validaDados(sexo,tipodado="sexo")
        nacionalidade_valida = validaDados(nacionalidade,tipodado="nacionalidade")
        if cpf_valido==True and nome_valido==True and idade_valida==True and sexo_valido==True and nacionalidade_valida==True:
            dados.append(cpf)
            dados.append(nome)
            dados.append(idade)
            dados.append(sexo)
            dados.append(nacionalidade)
            Cadastro(dados)
            try:
                opc=input("Deseja adicionar mais pessoas? s/n ")
                if opc=="s":
                    pass
                else:
                    break
            except ValueError:
                print("Não entendi se quer ou não. Auto destruição ativada!")
        

if __name__ == '__main__':
    main()


