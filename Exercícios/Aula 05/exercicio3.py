# ====================================================================================
# 3) Implemente uma função de consulta no programa do exercício 2.

import csv
import os

def Cadastro():
    cpf=""
    nome=""
    idade=""
    sexo=""
    endereco=""
    dados=[]
    cpf=input("Informe o CPF: ")
    nome=input("Informe o nome: ")
    idade=input("Informe a idade: ")
    sexo=input("Informe o sexo: (m/f) ")
    endereco=input("Informe o endereço: ")
    jahcadastrado = consultaUsuario(cpf)
    if jahcadastrado == False:
        dados.append(cpf)
        dados.append(nome)
        dados.append(idade)
        dados.append(sexo)
        dados.append(endereco)
        gravaDados(dados)

def gravaDados(dados):
    with open("registro2.csv","a",newline="") as arquivo:
        escrita = csv.writer(arquivo, delimiter=";")
        escrita.writerow(dados)
        os.system('cls')
        print(f"Cadastro realizado com sucesso!")
    arquivo.close()


def consultaUsuario(cpf):
    with open("registro2.csv","r") as arquivo:
        registros = csv.reader(arquivo, delimiter=";")
        for linha in registros:
            print(linha)
            if linha[0] == cpf:
                print(f"{cpf} já cadastrado.")
                #print(f"Dados: {linha}")
                print(f" - CPF: {linha[0]}")
                print(f" - Nome: {linha[1]}")
                print(f" - Idade: {linha[2]}")
                print(f" - Sexo: {linha[3]}")
                print(f" - Endereço: {linha[4]}")
                achou=True
                return achou
            else:
                achou = False
    return achou

def main():
    # cria o arquivo e imprime o header
    # with open("registro2.csv","w",newline="") as arquivo:
    #     escrita = csv.writer(arquivo, delimiter=";")
    #     escrita.writerow(["cpf,nome,idade,sexo,nacionalidade"])
    # arquivo.close()
    while True:
        #os.system('cls')
        linha="###########################################"
        print(linha)
        print(f"###    Painel de cadastro de usuário    ###")
        print(f"### Selecione uma das opções abaixo:    ###")
        print(f"### 1- Cadastrar novo usuário           ###")
        print(f"### 2- Consultar cadastro               ###")
        print(f"### 4- Sair                             ###")
        menu=input("")
        if menu=="1":
            Cadastro()
        elif menu=="2":
            print(linha)
            print(f"###    Painel de consulta de usuário    ###")
            print(f"### Informe os dados abaixo:            ###")
            print(f"###                                     ###")
            cpf=input("### CPF: ")
            consultaUsuario(cpf)
        else:
            break
     

if __name__ == '__main__':
    main()


