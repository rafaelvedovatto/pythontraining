# 1) Escreva um programa em python que conte as vogais que a música ‘Faroeste Caboclo’
# tem em sua letra. Armazena a letra da música em um arquivo do tipo txt.
# Dica: Não se esqueça de considerar as letras maiúsculas, minúsculas e com acentuação.

def ehVogal(letra):
    vogais = "aeiouáéíóúãõîôûâêîôûàèìòùäëïöü"
    if letra.lower() in vogais:
        return True
    else:
        return False
    
num_vogais = 0
consoantes=set()
with open("faroeste.txt","r",encoding="UTF-8") as arquivo:
    conteudo = arquivo.readlines()
    for linha in conteudo:
        for letra in linha:
            if ehVogal(letra) == True:
                num_vogais+=1
            else:
                consoantes.add(letra)
    print(f"Número de vogais: {num_vogais}")
    print(f"Letras não contadas: {consoantes}")
arquivo.close()

# with open("faroeste.txt","r",encoding="utf-8") as f:
#     linhas = f.read()
#     vogais="aeiouAEIOUáéíóúÁÉÍÓÚâêôÂÊÔãõÃÕ"
#     num_vogais = 0
#     print(linhas)
#     for letra in linhas:
#         if letra.lower() in vogais:
#             num_vogais+=1
#     print(f"Número de vogais: {num_vogais}")
# f.close()


